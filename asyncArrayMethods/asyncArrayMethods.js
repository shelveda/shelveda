const R = require("ramda");
// import * as R from "ramda";

const asyncForEach = R.curryN(2, (fn, arr) =>
  R.reduce(
    (promise, item) => promise.then(() => fn(item)),
    Promise.resolve()
  )(arr)
);

const asyncMap = R.curryN(2, (fn, arr) => Promise.all(arr.map(fn)));

const asyncFilter = (fn, arr) =>
  asyncMap(fn, arr).then((arr2) => arr.filter((v, i) => Boolean(arr2[i])));

const asyncReduce = (fn, init, arr) =>
  Promise.resolve(init).then((accum) =>
    asyncForEach(async (v, i) => {
      accum = await fn(accum, v, i);
    }, arr).then(() => accum)
  );

export const asyncFns = {
  map: asyncMap,
  forEach: asyncForEach,
  filter: asyncFilter,
  reduce: asyncReduce,
};
