import { asyncFns } from "./";

// helpers

const useResult = (x) => console.log(new Date(), x);
const fakeAPI = (delay, value) =>
  new Promise((resolve) => setTimeout(() => resolve(value), delay));

const fakeSum = (value1, value2) =>
  new Promise((resolve) => setTimeout(() => resolve(value1 + value2), 1000));

const fakeFilter = (value) =>
  new Promise((resolve) => setTimeout(() => resolve(value % 2 === 0), 1000));

// examples

const exampleAsycForeach = async () => {
  console.log("START FOREACH VIA REDUCE");

  const arr = [1, 2, 3, 4];
  await asyncFns.forEach(async (n) => {
    const x = await fakeAPI(n * 1000, n);
    useResult(x);
  })(arr);

  console.log("END FOREACH VIA REDUCE");
};

const exampleAsyncMap = async () => {
  console.log("START MAP");
  const arr = [1, 2, 3, 4];

  const mapped = await asyncFns.map(async (n) => {
    const x = await fakeAPI(n * 1000, n);
    return x;
  })(arr);

  useResult(mapped);
  console.log("END MAP");
};

const exampleAsyncFilter = async () => {
  console.log("START FILTER");

  const arr = [1, 2, 3, 4];

  const filtered = await asyncFns.filter(async (n) => {
    const x = await fakeFilter(n);
    return x;
  }, arr);
  useResult(filtered);
  console.log("END FILTER");
};

const exampleAsyncReduce = async () => {
  console.log("START REDUCE");

  const arr = [1, 2, 3, 4];

  const summed = await asyncFns.reduce(
    async (_accum, n) => {
      const accum = await _accum;
      const x = await fakeSum(accum, n);
      useResult(`accumulator=${accum} value=${x} `);
      return x;
    },
    0,
    arr
  );
  useResult(summed);
  console.log("END REDUCE");
};

export {
  exampleAsycForeach,
  exampleAsyncMap,
  exampleAsyncFilter,
  exampleAsyncReduce,
};
